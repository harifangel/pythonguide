								#BASICS ON MY PYTHON PROGRESS LEARNING


	#STRINGS

#DECLARATIONS
print("""				
			BASICS ON MY PYTHON PROGRESS LEARNING



	""")


print("""		STRINGS

	""")
#way to declare is with ""
message = "Message with quotation marks declaration."
print(message)
#You can also declare with ''
message2 = 'Message with single quotes declaration.'
print(message2)
#So you can do "'d'ew'ed'wdewdw"
#And this 'dwe"DW"EDE' so you cant get the "" '' characters in your string
#But if you want both '' and "" in the same string you can use triple quotes and 
#get your string as you like
message3 = """ ""''Message with triple quotation marks declaration.""''
"""
#it also respect the line jump
print(message3)


#DATA TYPE METHODS USUAGE

#so len() method return the length of the string (basic method)
print("length of string:",len(message))
#if you want to see and individual character of the string you can use square brackets on the variable
#and actually calls/create index of the variable and you can access to it
print("One index of string:",message[0])
# you can also access a range of characters of the string
# If i only want the word "Message" of variable message
print("A range of characters from the string:",message[0:7])
#note second parameter not count as part of the string return
#second parameter is the limit and its not included however firts it is included on the string return 
#You can aslo declare
print("			              ",message[:7])
#And also declare like this
print("				      ",message[29:])
print("""""")
#So if you want to lower case a string you can do it with .lower() method
print("Upper string converted to lower: ")
textupper = """ASDSAD"""
print(textupper.lower())
#and
print("Lower string converted to upper: ")
textlower = """asdsad"""
print(textlower.upper())
print("""""")
#count() method on string, count method recives one parameter and count how many times its this parameter
#on the string, is like a search of the string
print("""Counting how many elements of specific type have a string: message.count('Message') result =""",message.count('Message'))
print("						           message.count('a') Result =",message.count('a'))
#There is the find method and return the index on where is located what your looking if not return -1 value
print("""
Finding a string on our string: message.find('declaration') result = find on index(""",message.find('declaration'),""")
""")
#Another method is the replace, who replace characters from others of one string
print("""Repleacing fragments of string variable""")
#So here i create a string
greeting = "Hello world"
#The replace method recives two parameters the fragmet that want to replace and with what to replace
greeting.replace('world','universe')
#So here we can see that the replace does not have been made thats why you only delcare the change but 
#And does not is going to store in the original string you have to assign
print("Variable that it is going to be replace:",greeting)
#What you have to do is put it inside a print to get the result however like i said befor this is not affcting the
#original variable its
print("Variable fragment replaced:",greeting.replace('world','universe'))
#So if you want to make changes commit and store you have to create another variable and assign the replace
#or better reassign
greeting = greeting.replace('world','universe')
print("Second way to replace: "+greeting+""" 
""")
print("""Using Capitalize method (Capitalize the first element of the string):""")
#If you want to store use this way and if you want only the result use the other way 
#Use of capitalize() this capitalize first letter of string. 
name = "be 4 you"
print(name.capitalize())  
name1 = "be"
name2 = "4"
name3 = "you"
print(name1.capitalize() + name2.capitalize() 
                         + name3.capitalize(),"""
                         """) 



#cool and even more cool python tells you what methods are avalible for one specific variable you can use dir
#what this do give you a list of all method for the variable
print('Using dir command on string variable so i can see the methods that can be used.')
print("""List of methods that you can use on strings:""")
print(dir(greeting))
print("""""")
#If you want more information about what this methods do use the help command
#but you have to directly from the data type like this
#print(help(str))
#And for a spesific one like this:
#print(help(str.lower))
#And you can check the others data type
#print(help(int))
#print(help(bool))
#print(help(float))
#print(help(complex))

#Making use of each one of data type methods available
print("""
	Making use of each one of data type methods available (  Not in order:(  )

	""")
print("""   casefold()""")
#casefold returns the string lower case
upperstring="MINUSCULAS"
print(upperstring)
print(upperstring.casefold(),"""
	""")
print("   center()")
#center method center the string giving you space on both sides
centerString="==H=="
print(centerString)
print(centerString.center(24))
#And you can also fill the spaces
print(centerString.center(24,'$'),"""
	""")
print("""   endswith()""")
#The endswith() method returns True if a string ends with the given suffix otherwise returns False.
text = "madafaka nigga bitch." 
# returns False for the point at the end
result = text.endswith('nigga bitch') 
print (result)   
# returns True 
result = text.endswith('bitch.') 
print (result) 
# returns True 
result = text.endswith('nigga bitch.') 
print (result)  
# returns True 
result = text.endswith('madafaka nigga bitch.') 
print (result) 
print("")
print("""   expandtabs()""")
#this methods allows you to modify the tab size so you can separate the string how you want.
#but works this way
pigs="bacon\tbacon\tbacon"
print(pigs)
print(pigs.expandtabs(1))
print(pigs.expandtabs(2))
print(pigs.expandtabs(4))
print(pigs.expandtabs(20))
print("")
print("   ")
print("""   index()""")
#index method can recive 3 parametres and works same way find method works(returns the index where the parameter where found) 
#except returns error and you can be specific where index of string want to begin and end like if you only want to know a 
#specific part of string. 
fuckyou="fuck you"
print("String to verify:",fuckyou)
print("Word to verify inside string: you")
fuckyou1=fuckyou.index("you")
print(fuckyou1)
#if not find nothining throw error
#print("Second word: everybody")
#fuckyou2=fuckyou.index("everybody")
#print(fuckyou2s)(error)
fuckyou3=fuckyou.index("fuck",0,4)
#if you put like this ("Trust",0,3) gets error has to be the next position on string
print("fuck its present at:",fuckyou3)
print("")
print("""   isalnum()""")
#isalnum returns:
#True: If all the characters are alphanumeric
#False: If one or more characters are not alphanumeric
#here all characters are alpahnumeric
high="100porcent"
print(high.isalnum())
#here theres a space that is not alphanumric
slumb="as fuck"
print(slumb.isalnum())
print("")
print("""   isalpha""")
#The isalpha() methods returns “True” if all characters in the string are alphabets, Otherwise, It returns “False”.
#This function is used to check if the argument contains any alphabets characters such as :
#ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
snitch = "bitch"
print(snitch.isalpha())
bitch = "100porcentsnitch"
print(bitch.isalpha())
#if there spaces is going to do false
print("")
print("""   isdecimal()""")
#Python string method isdecimal() checks whether the string consists of only decimal characters. 
print("This method are present only on unicode objects.")
#Note − To define a string as Unicode, one simply prefixes a 'u' to the opening quotation mark of the assignment. 
asd = u"this2009";  
print(asd.isdecimal())
asd2 = u"23443434";
print(asd2.isdecimal())
print("")
print("""   isdigit()""")
#this method return true if al characters are digits
lol = "123456"  # Only digit in this string
print(lol.isdigit())
lol = "lol"
print(lol.isdigit())
print("""   isidentifier()""")
#The isidentifier() method returns True if the string is a valid identifier, otherwise False.
#A string is considered a valid identifier if it only contains alphanumeric letters (a-z) and (0-9), 
#or underscores (_). A valid identifier cannot start with a number, or contain any spaces.
a = "MyFolder"
b = "Demo002"
c = "2bring"
d = "my demo"
print(a, a.isidentifier())
print(b, b.isidentifier())
print(c, c.isidentifier())
print(d, d.isidentifier())
print("")
print("""   islower()""")
#this method return a true if all characteres are lowercase
a = "Bla bla bla, fuck you"
print(a, a.islower())
a = "bla bla bla, fuck you"
print(a, a.islower())
print("")
print("   isnumeric()")
#Python string method isnumeric() checks whether the string consists of only numeric characters. 
#This method is present only on unicode objects.
#Note − To define a string as Unicode, one simply prefixes a 'u' to the opening quotation mark of the assignment. 
#Below is the example.
b = u"fuckyou100"
print(b,b.isnumeric())
b=u"100"
print(b,b.isnumeric())
print("")
print("   isprintable()")
#Characters that occupies printing space on the screen are known as printable characters. For example:
#letters and symbols, digits, punctuation, whitespace
s = 'Fuck you with spaces :)'
print(s, s.isprintable())
s = '\nNew line and Fuck you is printable:'
print(s, s.isprintable())
s = ''
print('Empty string:', s.isprintable())
# written using ASCII
# chr(27) is escape character
# char(97) is letter 'a'
s = chr(27) + chr(97)
if s.isprintable() == True:
  print('Printable')
else:
  print('Not Printable')

s = '2+2 = 4'
if s.isprintable() == True:
  print('Printable')
else:
  print('Not Printable')
print("")
print("   isspace()")
a = " "
b = "        "
print(a,a.isspace())
print(b,b.isspace())
c = "This fuck you is not a space :)"
print(c,c.isspace())



#CONCATENATION
#I see that you can use "," and works like parameters and print with space between word like this
print("")
print("""Concatenation""")
#For "correct way" use + however this does not give you space between word so you have to put it
drink = 'beer'
print("First word: "+drink)
day = 'friday'
print("Second word: "+ day)
goodDay = day+' + ' + drink+ ' = chill'
print("Concatenated: ", goodDay)
#If you have a lot of variables you can create format so you dont get lose in the code 
#this is done adding brackets {} on where we want the variable
goodDay = '{} + {} = happiness'.format(day,drink)
print("Concatenated with format:", goodDay)
#Thanks to python there is a way more easire this is call f strings and can help with a lot
#So what this f strings allow you to do is put the variable directly between the brackets and not only thatXD
#It allows you to use methods on the varaible between the brackets like this:
fString = f'{drink} + {day.upper()} = good day'
print("""Concatenated with f strings and using methods on:""",fString)





print("""


	""")
	#INTEGERS AND FLOATS
#
print("""		INTEGERS AND FLOATS

	""")
a = 365
#with 'type' command you get the type string of the object
print(type(a))
#float
e = 123.231
print(type(e))
#complex 
z = 2 - 6.1j
print(type(z))
print(z.real)
print(z.imag)